#include <ncurses.h>
#include <pthread.h>
#include <unistd.h>
#include <cstdlib>
#include <time.h> 

#define NORMAL_ROAD 0
#define INTERSECTION 1
#define OUTSIDE 2

#define LEFT 0
#define STRAIGHT 1
#define RIGHT 2

class Car;

struct road_block{
	int type;
	Car* car = NULL;
	pthread_cond_t* cond = NULL;
 	pthread_mutex_t* mutex = NULL;
};
const int map_N = 16;
volatile road_block map[map_N][map_N];

bool end = false;

class Car{
	public:
		char sign;
		int x;
		int y;
		int v_x;
		int v_y;
		int next_dir;
		pthread_cond_t cond;
		pthread_mutex_t mutex;

	Car(int* tab){
		sign = rand()%('Z'-'A' + 1) + 'A'; 
		x = tab[0];
		y = tab[1];
		v_x = tab[2];
		v_y = tab[3];
		next_dir = tab[4];

		pthread_cond_init(&cond, NULL);
		pthread_mutex_init(&mutex, NULL);
	}

	static void* move(void* ptr_v){
		Car* ptr = (Car*) ptr_v;
		int step = 0;
		while(!end){
			sleep(1);
			if(step != 0){ //going throught intersection
				switch(ptr->next_dir){
					case STRAIGHT:
						if(step == 1){
							//go straight
							int x_new = ptr->x + ptr->v_x;
							int y_new = ptr->y + ptr->v_y;
							map[ptr->x][ptr->y].car = NULL;
							map[x_new][y_new].car = ptr;
							ptr->x = x_new;
							ptr->y = y_new;
							step = 2;
						}
						else{
							//go straight
							int x_new = ptr->x + ptr->v_x;
							int y_new = ptr->y + ptr->v_y;
							map[ptr->x][ptr->y].car = NULL;
							map[x_new][y_new].car = ptr;
							//send signal to intersection
							pthread_mutex_lock(map[ptr->x][ptr->y].mutex);
							pthread_cond_signal(map[ptr->x][ptr->y].cond);
							pthread_mutex_unlock(map[ptr->x][ptr->y].mutex);
							ptr->x = x_new;
							ptr->y = y_new;
							//randomize next dirextion
							ptr->next_dir = rand() % 3;
							step = 0;
						}
						break;
					case LEFT:
						if(step == 1){
							//go straight
							int x_new = ptr->x + ptr->v_x;
							int y_new = ptr->y + ptr->v_y;
							map[ptr->x][ptr->y].car = NULL;
							map[x_new][y_new].car = ptr;
							ptr->x = x_new;
							ptr->y = y_new;
							step = 2;
						}
						else{
							if(step == 2){
								//turn left
								if(ptr->v_x != 0){
									ptr->v_x = -ptr->v_x;
								}
								int temp = ptr->v_y;
								ptr->v_y = ptr->v_x;
								ptr->v_x = temp;
								//go straight
								int x_new = ptr->x + ptr->v_x;
								int y_new = ptr->y + ptr->v_y;
								map[ptr->x][ptr->y].car = NULL;
								map[x_new][y_new].car = ptr;
								ptr->x = x_new;
								ptr->y = y_new;
								step = 3;
							}
							else{
								//go straight
								int x_new = ptr->x + ptr->v_x;
								int y_new = ptr->y + ptr->v_y;
								map[ptr->x][ptr->y].car = NULL;
								map[x_new][y_new].car = ptr;
								int x_old = ptr->x;
								int y_old = ptr->y;
								ptr->x = x_new;
								ptr->y = y_new;
								//randomize next dirextion
								ptr->next_dir = rand() % 3;
								//send signal to intersection
								pthread_mutex_lock(map[x_old][y_old].mutex);
								pthread_cond_signal(map[x_old][y_old].cond);
								pthread_mutex_unlock(map[x_old][y_old].mutex);
								step = 0;
							}
						}
						break;
					case RIGHT:
						//turn right
						if(ptr->v_x == 0){
							ptr->v_y = -ptr->v_y;
						}
						int temp = ptr->v_y;
						ptr->v_y = ptr->v_x;
						ptr->v_x = temp;
						//go straight
						int x_new = ptr->x + ptr->v_x;
						int y_new = ptr->y + ptr->v_y;
						map[ptr->x][ptr->y].car = NULL;
						map[x_new][y_new].car = ptr;
						//send signal to intersection
						pthread_mutex_lock(map[ptr->x][ptr->y].mutex);
						pthread_cond_signal(map[ptr->x][ptr->y].cond);
						pthread_mutex_unlock(map[ptr->x][ptr->y].mutex);
						ptr->x = x_new;
						ptr->y = y_new;
						//randomize next dirextion
						ptr->next_dir = rand() % 3;
						step = 0;
				}
			}
			else{
				//calculate next road block location
				int x_new = ptr->x + ptr->v_x;
				int y_new = ptr->y + ptr->v_y;
				if(x_new <= -1 || x_new >= map_N || y_new <= -1 || y_new >= map_N){
					//remove car
					map[ptr->x][ptr->y].car = NULL;
					break;
				}
				//check next road block type
				int type = map[x_new][y_new].type;
				if(type == NORMAL_ROAD){
					if(map[x_new][y_new].car == NULL){
						map[ptr->x][ptr->y].car = NULL;
						map[x_new][y_new].car = ptr;
						ptr->x = x_new;
						ptr->y = y_new;
					}
				}
				else{
					pthread_mutex_lock(map[x_new][y_new].mutex);
       				pthread_cond_signal(map[x_new][y_new].cond);
     				pthread_mutex_unlock(map[x_new][y_new].mutex);
					
					pthread_mutex_lock(&ptr->mutex);
     				pthread_cond_wait(&ptr->cond, &ptr->mutex);
					//allowed to go by intersection
					switch(ptr->next_dir){
						case STRAIGHT:
							map[ptr->x][ptr->y].car = NULL;
							map[x_new][y_new].car = ptr;
							ptr->x = x_new;
							ptr->y = y_new;
							step = 1;
							break;
						case LEFT:
							map[ptr->x][ptr->y].car = NULL;
							map[x_new][y_new].car = ptr;
							ptr->x = x_new;
							ptr->y = y_new;
							step = 1;
							break;
						case RIGHT:
							map[ptr->x][ptr->y].car = NULL;
							map[x_new][y_new].car = ptr;
							ptr->x = x_new;
							ptr->y = y_new;
							step = 1;
					}
     				pthread_mutex_unlock(&ptr->mutex);
				}
			}
		}
	}
};

void* intersection(void* tab_v){
	int* tab = (int*) tab_v;
	int x = tab[0];
	int y = tab[1];
	//create cond_var and mutex and assign to 4 blocks
	pthread_cond_t cond;
	pthread_mutex_t mutex;
  	pthread_cond_init(&cond, NULL);
  	pthread_mutex_init(&mutex, NULL);
	for(int i = 0; i <= 1; ++i){
		for(int j = 0; j <= 1; ++j){
			map[x+i][y+j].cond = &cond;
			map[x+i][y+j].mutex = &mutex;
		}
	}

	pthread_mutex_lock(&mutex);
	while (!end){
		//wait until car gets to the intersection or leave it
		pthread_cond_wait(&cond, &mutex);
		//check if intersection is empty
		if(map[x][y].car == NULL
		&& map[x+1][y].car == NULL
		&& map[x][y+1].car == NULL
		&& map[x+1][y+1].car == NULL){
			//// 1 /////
			// 0   2  //
			//// 3 /////
			
			//create arrays with pos. of cars
			int tab_x[4] = {x-1, x, x+2, x+1};
			int tab_y[4] = {y+1, y-1, y, y+2};

			bool stalemate = true; //4 cars going straight/left
			for(int i=0; i<4; ++i){
				//check if there's a car on pos. i
				int i_x = tab_x[i];
				int i_y = tab_y[i];
				Car* car = map[i_x][i_y].car;
				if(car != NULL){
					//there's a car on pos. i
					if(car->next_dir == RIGHT){
						//car going right
						//check if there's space for movement
						int vtemp_x = car->v_y;
						int vtemp_y = car->v_x; 
						if(car->v_x == 0){
							vtemp_x = -car->v_y;
						}
						int x_new = car->x + car->v_x + vtemp_x;
						int y_new = car->y + car->v_y + vtemp_y;
						if(map[x_new][y_new].car == NULL){
							//there's space
							//send signal for car to go
							pthread_mutex_lock(&car->mutex);
							pthread_cond_signal(&car->cond);
							pthread_mutex_unlock(&car->mutex);
							stalemate = false;
							break;
						}
						continue;
					}
					if(car->next_dir == STRAIGHT){
						//car going straight
						//check if a there's car on pos. j=i-1 (car's right)
						int j = i-1;
						if(j < 0) j = 3;
						int j_x = tab_x[j];
						int j_y = tab_y[j];
						Car* car_right = map[j_x][j_y].car;
						if(car_right == NULL){
							//no car on pos. j=i-1 (car's right)
							//check if there's space for movement
							int x_new = car->x + 3 * car->v_x;
							int y_new = car->y + 3 * car->v_y;
							if(map[x_new][y_new].car == NULL){
								//there's space
								//send signal for car to go
								pthread_mutex_lock(&car->mutex);
								pthread_cond_signal(&car->cond);
								pthread_mutex_unlock(&car->mutex);
								stalemate = false;
								break;
							}
						} 
						continue;
					}
					if(car->next_dir == LEFT){
						//car going left
						//check if there's a car on pos. j=i-1 (car's right)
						int j = i-1;
						if(j < 0) j = 3;
						int j_x = tab_x[j];
						int j_y = tab_y[j];
						Car* car_right = map[j_x][j_y].car;
						if(car_right == NULL){
							//no car on pos. j=i-1 (car's right)
							//check if there's a car on pos. j=i+2 (in front of car)
							j = i+2;
							if(j > 3) j -= 4;
							j_x = tab_x[j];
							j_y = tab_y[j];
							Car* car_infront = map[j_x][j_y].car;
							if(car_infront == NULL){
								//no car on pos. j=i+2 (in front of car)
								//check if there's space for movement
								int vtemp_x = car->v_y;
								int vtemp_y = car->v_x;
								if(car->v_x != 0){
									vtemp_y = -car->v_x;
								}
								int x_new = car->x + 2 * (car->v_x + vtemp_x);
								int y_new = car->y + 2 * (car->v_y + vtemp_y);
								if(map[x_new][y_new].car == NULL){
									//there's space
									//send signal for car to goright
									pthread_mutex_lock(&car->mutex);
									pthread_cond_signal(&car->cond);
									pthread_mutex_unlock(&car->mutex);
									stalemate = false;
									break;
								}	
							}
						}
					}
				}
			}
			if(stalemate){ 
				//solve problem - 4 cars going straight/left
				for(int i = 0; i < 4; ++i){
					int i_x = tab_x[i];
					int i_y = tab_y[i];
					Car* car = map[i_x][i_y].car;
					if(car != NULL){
						//there's a car on pos. i
						if(car->next_dir == RIGHT){
							//car going right
							//check if there's space for movement
							int vtemp_x = car->v_y;
							int vtemp_y = car->v_x;
							if(car->v_x == 0){
								vtemp_x = -car->v_y;
							}
							int x_new = car->x + car->v_x + vtemp_x;
							int y_new = car->y + car->v_y + vtemp_y;
							if(map[x_new][y_new].car == NULL){
								//there's space
								//send signal for car to go
								pthread_mutex_lock(&car->mutex);
								pthread_cond_signal(&car->cond);
								pthread_mutex_unlock(&car->mutex);
								stalemate = false;
								break;
							}
							continue;
						}
						if(car->next_dir == STRAIGHT){
							//car going straight
							//check if there's space for movement
							int x_new = car->x + 3 * car->v_x;
							int y_new = car->y + 3 * car->v_y;
							if(map[x_new][y_new].car == NULL){
								//there's space
								//send signal for car to go
								pthread_mutex_lock(&car->mutex);
								pthread_cond_signal(&car->cond);
								pthread_mutex_unlock(&car->mutex);
								stalemate = false;
								break;
							}
							continue;
						}
						if(car->next_dir == LEFT){
							//car going left
							//check if there's space for movement
							int vtemp_x = car->v_y;
							int vtemp_y = car->v_x;
							if(car->v_x != 0){
								vtemp_y = -car->v_x;
							}
							int x_new = car->x + 2 * (car->v_x + vtemp_x);
							int y_new = car->y + 2 * (car->v_y + vtemp_y);
							if(map[x_new][y_new].car == NULL){
								//there's space
								//send signal for car to goright
								pthread_mutex_lock(&car->mutex);
								pthread_cond_signal(&car->cond);
								pthread_mutex_unlock(&car->mutex);
								stalemate = false;
								break;
							}	
						}
					}
				}
				
			}
		}
	}
	pthread_mutex_unlock(&mutex);

}

void* draw(void* x){
	initscr();
	curs_set(0);
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_GREEN); //outside
	init_pair(2, COLOR_BLACK, COLOR_BLACK); //empty road
	init_pair(3, COLOR_BLACK, COLOR_RED); //car going left
	init_pair(4, COLOR_BLACK, COLOR_WHITE); //car going straight
	init_pair(5, COLOR_BLACK, COLOR_BLUE); //car going right

	timeout(1);

	while(!end){
		if(getch()=='x') end = true;

		for(int i = 0; i < map_N; ++i){
			for(int j = 0; j < map_N; ++j){
				int type = map[i][j].type;
				if(type == OUTSIDE){
					attron(COLOR_PAIR(1));
					mvprintw(j, i, " ");
				}
				else {
					Car* car = map[i][j].car;
					if(car == NULL){
						attron(COLOR_PAIR(2));
						mvprintw(j, i, " ");
					}
					else{
						switch(car->next_dir){
							case LEFT:
								attron(COLOR_PAIR(3));
								break;
							case STRAIGHT:
								attron(COLOR_PAIR(4));
								break;
							case RIGHT:
								attron(COLOR_PAIR(5));
						}
						mvprintw(j, i, &car->sign);
					}
				}
			}
		}

		refresh();
		usleep(40000);
	}
	endwin();
}

void* car_generator(void* ptr){
	srand(time(NULL));
	usleep(100000);
	while(!end){
		pthread_t thread;
		int tab[5];
		do{
			switch(rand() % 8){
				case 0:
					tab[0] = 4; //x
					tab[1] = 0; //y
					tab[2] = 0; //v_x
					tab[3] = 1; //v_y
					break;
				case 1:
					tab[0] = 10; //x
					tab[1] = 0; //y
					tab[2] = 0; //v_x
					tab[3] = 1; //v_y
					break;
				case 2:
					tab[0] = 15; //x
					tab[1] = 4; //y
					tab[2] = -1; //v_x
					tab[3] = 0; //v_y
					break;
				case 3:
					tab[0] = 15; //x
					tab[1] = 10; //y
					tab[2] = -1; //v_x
					tab[3] = 0; //v_y
					break;
				case 4:
					tab[0] = 11; //x
					tab[1] = 15; //y
					tab[2] = 0; //v_x
					tab[3] = -1; //v_y
					break;
				case 5:
					tab[0] = 5; //x
					tab[1] = 15; //y
					tab[2] = 0; //v_xx
					tab[3] = -1; //v_y
					break;
				case 6:
					tab[0] = 0; //x
					tab[1] = 11; //y
					tab[2] = 1; //v_x
					tab[3] = 0; //v_y
					break;
				case 7:
					tab[0] = 0; //x
					tab[1] = 5; //y
					tab[2] = 1; //v_x
					tab[3] = 0; //v_y
			}
		} while(map[tab[0]][tab[1]].car != NULL);
		tab[4] = rand()%3; //next_dir
		Car* car = new Car(tab);
		map[tab[0]][tab[1]].car = car;
		
		pthread_create(&thread, NULL, car->move, (void*)car);
		sleep(2);
	}	
}

void initialize_map(){
	for(int i = 0; i < 8; ++i){
		for(int j = 0; j < 8; ++j){
			for(int k = 0; k < 2; ++k){
				for(int l = 0; l < 2; ++l){
					if(i == 2 || i == 5){
						if(j == 2 || j == 5){
							map[i * 2 + k][j * 2 + l].type = INTERSECTION;
						}
						else{
							map[i * 2 + k][j * 2 + l].type = NORMAL_ROAD;
						}
					}
					else{
						if(j == 2 || j == 5){
							map[i * 2 + k][j * 2 + l].type = NORMAL_ROAD;
						}
						else{
							map[i * 2 + k][j * 2 + l].type = OUTSIDE;
						}
					}
				}
			}
		}
	}
	int tab_1[2] = {4,4};
	pthread_t thread_inter1;
	pthread_create(&thread_inter1, NULL, intersection, (void*)tab_1);

	int tab_2[2] = {10,4};
	pthread_t thread_inter2;
	pthread_create(&thread_inter2, NULL, intersection, (void*)tab_2);

	int tab_3[2] = {10,10};
	pthread_t thread_inter3;
	pthread_create(&thread_inter3, NULL, intersection, (void*)tab_3);

	int tab_4[2] = {4,10};
	pthread_t thread_inter4;
	pthread_create(&thread_inter4, NULL, intersection, (void*)tab_4);
}

void* intersection_refresh(void* t){
	int tab_x[4] = {4, 10, 10, 4};
	int tab_y[4] = {4, 4, 10, 10};
	while(!end){
		for(int i = 0; i < 4; ++i){
			int x = tab_x[i];
			int y = tab_y[i];
			pthread_mutex_lock(map[x][y].mutex);
			pthread_cond_signal(map[x][y].cond);
			pthread_mutex_unlock(map[x][y].mutex);
		}
		sleep(3);
	}
}

int main(){
	initialize_map();
	sleep(1);

	pthread_t thread, thread2, thread3;
	pthread_create(&thread, NULL, draw, (void*)new int);
	pthread_create(&thread2, NULL, car_generator, (void*)new int);
	pthread_create(&thread3, NULL, intersection_refresh, (void*)new int);

	pthread_join(thread, NULL);
	pthread_join(thread2, NULL);
	pthread_join(thread3, NULL);

	return 0;
}